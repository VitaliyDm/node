const router = require('express').Router();
const express = require('express');
const path = require('path');

const auth = require('./auth');
const user = require('./user');

const authorize = require('../middlewares/authorize');
const HttpError = require('../errors/HttpError');
const status = require('../middlewares/statusHandler');
const images = require('../middlewares/imageListHandler');
const userModel = require('../models/user');

const services = {
    models: {
        user: userModel,
    },
};

router
    .use('/status', status)
    .use('/images', images)
    .use('/auth', auth(services))
    .use('/user', authorize, user(services))
    .use('/static', express.static(path.join(__dirname, '../data')))
    .all('*', () => { throw new HttpError(405); });

module.exports = router;
