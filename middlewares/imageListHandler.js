const fs = require('fs');
const path = require('path');
const imagesFolder = path.join(__dirname, '../data/images');
const HttpError = require('../errors/HttpError');

module.exports = (request, response) => {
    fs.readdir(imagesFolder, (err, files) => {
        if (err) {
            throw new HttpError(500);
        }
        response.json({
            images: files.filter(image => /\d+\.png/.test(image)),
            status: 'ok',
        });
    });
};
