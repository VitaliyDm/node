var mongoose = require('mongoose');
var ModelUser = require('./models/user');

var mongoDB = 'mongodb://localhost:27017';

mongoose.Promise = global.Promise;
mongoose.connect(mongoDB);

var db = mongoose.connection;

// eslint-disable-next-line no-console
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var newUser = new ModelUser({
    name: 'User',
    email: 'exapmle@example.com'  ,
});

newUser.save(function(err) {
    if (err) throw err;

    // eslint-disable-next-line no-console
    console.log('User created!');
});

ModelUser.find({}, function(err, users) {
    if (err) throw err;

    // eslint-disable-next-line no-console
    console.log(users);
});
